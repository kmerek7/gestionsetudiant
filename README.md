# GestionEtudiant

---

### Requirements

---

- Apache 2.4
- PHP 7.3
- MySQL 5.7
- Composer

### Usage

---

### Installation

---

```
git clone https://gitlab.com/kmerek7/gestionsetudiant.git
cd gestionsetudiant
faire composer install pour installer toute les dependances
```

### Configuration database

---

```
# retirer le # devant la ligne Mysql dans le fichier .env
# modifier le usernam et mots de passe de la bd si neccesaire
# Créer la base de donnée 'GestionEtudiant' si cette base n'hesite pas encore
# -f signifie --force pour force l'excecution
- bin/console doctrine:database:create -f

# met a jour les entites en base de donnée
- bin/console doctrine:schema:update -f

# Lance les fixtures pour avoir des données de test en base
- bin/console doctrine:fixtures:load
```
