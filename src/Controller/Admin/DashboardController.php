<?php

namespace App\Controller\Admin;

use App\Entity\Etudiant;
use App\Entity\Evaluation;
use App\Entity\Filiere;
use App\Entity\Note;
use App\Entity\Semestre;
use App\Entity\Ue;
use App\Entity\UeFiliere;
use App\Repository\EtudiantRepository;
use App\Repository\FiliereRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractDashboardController
{
    protected $etudiant;
    protected $filiere;

    public function __construct(EtudiantRepository $etudiant, FiliereRepository $filiere)
    {
        $this->etudiant = $etudiant;
        $this->filiere = $filiere;
    }
    public function index(): Response
    {
        return $this->render(
            'admin/dashboard.html.twig',
            [
                'Countetudiant' => $this->etudiant->Countetudiant(),
                'Countfiliere' => $this->filiere->Countfiliere()
            ]
        );
        // return $this->render('admin/formtheme.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Gestionsetudiant');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Acceuil');
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::section('Classe');
        yield MenuItem::subMenu('Etudiant', 'fa fa-id-card-o')->setSubItems(
            [
                MenuItem::linkToCrud('Ajouter un étudiant', 'fa fa-user-plus', Etudiant::class)->setAction('new'),
                MenuItem::linkToCrud('Liste des étudiants', 'fas fa-list', Etudiant::class),

            ]
        );
        yield MenuItem::subMenu('Filiere', 'fas fa-list')->setSubItems(
            [
                MenuItem::linkToCrud('Ajouter une filiere', 'fa fa-plus-square', Filiere::class)->setAction('new'),
                MenuItem::linkToCrud('Liste des filieres', 'fas fa-file-pdf', Filiere::class),

            ]
        );
        yield MenuItem::section('Etude');
        yield MenuItem::subMenu('Unite d\'enseignement', 'fa fa-tasks')->setSubItems(
            [
                MenuItem::linkToCrud('Ajouter une Ue', 'fa fa-plus-square', Ue::class)->setAction('new'),
                MenuItem::linkToCrud('Liste des Ues', 'fa fa-list', Ue::class)

            ]
        );
        yield MenuItem::subMenu('Note', 'fa fa-pencil-square-o')->setSubItems(
            [
                MenuItem::linkToCrud('Ajouter une note', 'fa fa-plus-square', Note::class)->setAction('new'),
                MenuItem::linkToCrud('Liste des notes', 'fa fa-list', Note::class)
            ]
        );
        yield MenuItem::subMenu('Ue filiere', 'fa fa-sticky-note')->setSubItems(
            [
                MenuItem::linkToCrud('Ajouter une uefiliere', 'fa fa-plus-square', UeFiliere::class)->setAction('new'),
                MenuItem::linkToCrud('liste des Ue Filiere', 'fas fa-list', UeFiliere::class)

            ]
        );

        yield MenuItem::section('Composition');
        yield MenuItem::subMenu('Evaluation', 'fa fa-file')->setSubItems(
            [
                MenuItem::linkToCrud('Evaluation', 'fa fa-plus-square', Evaluation::class)->setAction('new'),
                MenuItem::linkToCrud('Evaluation', 'fas fa-list', Evaluation::class)

            ]
        );
        yield MenuItem::linkToCrud('Semestre', 'fas fa-list', Semestre::class);
    }
}
