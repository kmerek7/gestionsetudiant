<?php

namespace App\Controller\Admin;

use App\Entity\Note;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class NoteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Note::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Note')
                ->setIcon('phone')->addCssClass('optional')
                ->setTextAlign('center'),
            TextField::new('mention'),
            AssociationField::new('etudiants'),
            AssociationField::new('uefilieres'),
            NumberField::new('note20')->setCssClass('color', 'red'),
            TextField::new('mention'),

            AssociationField::new('evaluations'),
            AssociationField::new('uefilieres', 'Unité d\'enseignement')


        ];
    }
}
