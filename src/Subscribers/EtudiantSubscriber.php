<?php

namespace App\Subscribers;

use App\Entity\Etudiant;
use App\Repository\EtudiantRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EtudiantSubscriber implements EventSubscriberInterface
{
    private $lastid;


    public function __construct(EtudiantRepository $lastid)
    {
        $this->lastid = $lastid;
        // $okk = implode($et->findLastid());
        // $ok = 'gm';
        // printf($okk);
        // dd();
    }
    public static function getSubscribedEvents()
    {
        return
            [
                BeforeEntityPersistedEvent::class => ['setMatricule']
            ];
    }
    public function setMatricule(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Etudiant) {
            $id = implode($this->lastid->findLastid());
            $code = $entity->getFilieres()->getcodeFil();
            $date = $entity->getDateNais()->format('Y');
            $now = date('Y');
            if ($now - $date > 14) {
                $entity->setMatricule($code . $id);
            } else {
                throw new \Exception('Date Invalide');
            }
        }
    }
}
