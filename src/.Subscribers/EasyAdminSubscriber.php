<?php

namespace App\EventSubscriber;

use App\Entity\Etudiant;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminSubsceiber implements EventSubscriberInterface
{

    private $filiere;

    public function __construct()
    {
    }
    public static function getSubscribedEvents()
    {
        return
            [
                BeforeEntityPersistedEvent::class => ['setfiliere'],
            ];
    }
    public function setfiliere(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Etudiant)) {
            return;
        }
    }
}
