<?php

namespace App\Entity;

use App\Repository\NoteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 */
class Note
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $note20;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $mention;

    /**
     * @ORM\ManyToOne(targetEntity=Etudiant::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $etudiants;

    /**
     * @ORM\ManyToOne(targetEntity=Evaluation::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $evaluations;

    /**
     * @ORM\ManyToOne(targetEntity=UeFiliere::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $uefilieres;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote20(): ?float
    {
        return $this->note20;
    }

    public function setNote20(float $note20): self
    {
        $this->note20 = $note20;

        return $this;
    }

    public function getMention(): ?string
    {
        return $this->mention;
    }

    public function setMention(string $mention): self
    {
        $this->mention = $mention;

        return $this;
    }

    public function getEtudiants(): ?Etudiant
    {
        return $this->etudiants;
    }

    public function setEtudiants(?Etudiant $etudiants): self
    {
        $this->etudiants = $etudiants;

        return $this;
    }

    public function getEvaluations(): ?Evaluation
    {
        return $this->evaluations;
    }

    public function setEvaluations(?Evaluation $evaluations): self
    {
        $this->evaluations = $evaluations;

        return $this;
    }

    public function getUefilieres(): ?UeFiliere
    {
        return $this->uefilieres;
    }

    public function setUefilieres(?UeFiliere $uefilieres): self
    {
        $this->uefilieres = $uefilieres;

        return $this;
    }
}
