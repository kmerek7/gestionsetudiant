<?php

namespace App\Entity;

use App\Entity\Filiere;
use App\Repository\UeFiliereRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UeFiliereRepository::class)
 */
class UeFiliere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Note::class, mappedBy="uefilieres", orphanRemoval=true)
     */
    private $notes;

    /**
     * @ORM\ManyToOne(targetEntity=Semestre::class, inversedBy="ueFilieres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $semestres;

    /**
     * @ORM\ManyToOne(targetEntity=Ue::class, inversedBy="ueFilieres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Ues;

    /**
     * @ORM\ManyToOne(targetEntity=Filiere::class, inversedBy="ueFilieres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $filieres;



    public function __construct()
    {
        $this->notes = new ArrayCollection();
        $this->notesUefiliers = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->getUes()->__toString();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setUefilieres($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getUefilieres() === $this) {
                $note->setUefilieres(null);
            }
        }

        return $this;
    }

    public function getSemestres(): ?Semestre
    {
        return $this->semestres;
    }

    public function setSemestres(?Semestre $semestres): self
    {
        $this->semestres = $semestres;

        return $this;
    }

    public function getUes(): ?Ue
    {
        return $this->Ues;
    }

    public function setUes(?Ue $Ues): self
    {
        $this->Ues = $Ues;

        return $this;
    }

    public function getFilieres(): ?Filiere
    {
        return $this->filieres;
    }

    public function setFilieres(?Filiere $filieres): self
    {
        $this->filieres = $filieres;

        return $this;
    }
}
