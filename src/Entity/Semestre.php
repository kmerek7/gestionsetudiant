<?php

namespace App\Entity;

use App\Repository\SemestreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SemestreRepository::class)
 */
class Semestre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $libelleSem;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     */
    private $dateFin;

    /**
     * @ORM\OneToMany(targetEntity=UeFiliere::class, mappedBy="semestres", orphanRemoval=true)
     */
    private $ueFilieres;

    public function __construct()
    {
        $this->ueFilieres = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getLibelleSem();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleSem(): ?string
    {
        return $this->libelleSem;
    }

    public function setLibelleSem(string $libelleSem): self
    {
        $this->libelleSem = $libelleSem;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * @return Collection|UeFiliere[]
     */
    public function getUeFilieres(): Collection
    {
        return $this->ueFilieres;
    }

    public function addUeFiliere(UeFiliere $ueFiliere): self
    {
        if (!$this->ueFilieres->contains($ueFiliere)) {
            $this->ueFilieres[] = $ueFiliere;
            $ueFiliere->setSemestres($this);
        }

        return $this;
    }

    public function removeUeFiliere(UeFiliere $ueFiliere): self
    {
        if ($this->ueFilieres->removeElement($ueFiliere)) {
            // set the owning side to null (unless already changed)
            if ($ueFiliere->getSemestres() === $this) {
                $ueFiliere->setSemestres(null);
            }
        }

        return $this;
    }
}
