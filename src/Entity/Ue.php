<?php

namespace App\Entity;

use App\Repository\UeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UeRepository::class)
 */
class Ue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $libelleUe;

    /**
     * @ORM\Column(type="integer")
     */
    private $credit;

    /**
     * @ORM\OneToMany(targetEntity=UeFiliere::class, mappedBy="Ues", orphanRemoval=true)
     */
    private $ueFilieres;

    public function __construct()
    {
        $this->ueFilieres = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getLibelleUe();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLibelleUe(): ?string
    {
        return $this->libelleUe;
    }

    public function setLibelleUe(string $libelleUe): self
    {
        $this->libelleUe = $libelleUe;

        return $this;
    }

    public function getCredit(): ?int
    {
        return $this->credit;
    }

    public function setCredit(int $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * @return Collection|UeFiliere[]
     */
    public function getUeFilieres(): Collection
    {
        return $this->ueFilieres;
    }

    public function addUeFiliere(UeFiliere $ueFiliere): self
    {
        if (!$this->ueFilieres->contains($ueFiliere)) {
            $this->ueFilieres[] = $ueFiliere;
            $ueFiliere->setUes($this);
        }

        return $this;
    }

    public function removeUeFiliere(UeFiliere $ueFiliere): self
    {
        if ($this->ueFilieres->removeElement($ueFiliere)) {
            // set the owning side to null (unless already changed)
            if ($ueFiliere->getUes() === $this) {
                $ueFiliere->setUes(null);
            }
        }

        return $this;
    }
}
