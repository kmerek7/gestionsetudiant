<?php

namespace App\Entity;

use App\Repository\FiliereRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FiliereRepository::class)
 */
class Filiere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $libelleFil;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $codeFil;

    /**
     * @ORM\OneToMany(targetEntity=Etudiant::class, mappedBy="filieres", orphanRemoval=true)
     */
    private $etudiants;

    /**
     * @ORM\OneToMany(targetEntity=UeFiliere::class, mappedBy="filieres", orphanRemoval=true)
     */
    private $ueFilieres;

    public function __construct()
    {
        $this->etudiants = new ArrayCollection();
        $this->ueFilieres = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getLibelleFil();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleFil(): ?string
    {
        return $this->libelleFil;
    }

    public function setLibelleFil(string $libelleFil): self
    {
        $this->libelleFil = $libelleFil;

        return $this;
    }

    public function getcodeFil(): ?string
    {
        return $this->codeFil;
    }

    public function setcodeFil(string $codeFil): self
    {
        $this->codeFil = $codeFil;

        return $this;
    }

    /**
     * @return Collection|Etudiant[]
     */
    public function getEtudiants(): Collection
    {
        return $this->etudiants;
    }

    public function addEtudiant(Etudiant $etudiant): self
    {
        if (!$this->etudiants->contains($etudiant)) {
            $this->etudiants[] = $etudiant;
            $etudiant->setFilieres($this);
        }

        return $this;
    }

    public function removeEtudiant(Etudiant $etudiant): self
    {
        if ($this->etudiants->removeElement($etudiant)) {
            // set the owning side to null (unless already changed)
            if ($etudiant->getFilieres() === $this) {
                $etudiant->setFilieres(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UeFiliere[]
     */
    public function getUeFilieres(): Collection
    {
        return $this->ueFilieres;
    }

    public function addUeFiliere(UeFiliere $ueFiliere): self
    {
        if (!$this->ueFilieres->contains($ueFiliere)) {
            $this->ueFilieres[] = $ueFiliere;
            $ueFiliere->setFilieres($this);
        }

        return $this;
    }

    public function removeUeFiliere(UeFiliere $ueFiliere): self
    {
        if ($this->ueFilieres->removeElement($ueFiliere)) {
            // set the owning side to null (unless already changed)
            if ($ueFiliere->getFilieres() === $this) {
                $ueFiliere->setFilieres(null);
            }
        }

        return $this;
    }
}
