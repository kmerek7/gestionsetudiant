<?php

namespace App\DataFixtures;

use App\Entity\Etudiant;
use App\Entity\Evaluation;
use App\Entity\Filiere;
use App\Entity\Ue;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');

        $user = new User();

        $user->setUsername('admin');

        $password = $this->encoder->encodePassword($user, 'admin');
        $user->setPassword($password);
        $manager->persist($user);

        $filiere = new Filiere();
        $filiere->setcodeFil('GL')
            ->setLibelleFil('Genie Logiciel');
        $manager->persist($filiere);
        $filiere2 = new Filiere();
        $filiere2->setcodeFil('GC')
            ->setLibelleFil('Genie Civil');
        $filiere3 = new Filiere();
        $filiere3->setcodeFil('GRH')
            ->setLibelleFil('GestionResourcesHumains');
        $filiere4 = new Filiere();
        $filiere4->setcodeFil('EQV')
            ->setLibelleFil('EnvironementQualitevie');
        $filiere5 = new Filiere();
        $filiere5->setcodeFil('RT')
            ->setLibelleFil('Reseaux Et Communication');
        $manager->persist($filiere2);
        $manager->persist($filiere3);
        $manager->persist($filiere4);
        $manager->persist($filiere5);
        for ($i = 1; $i < 6; $i++) {
            $etudiant = new Etudiant();
            $matricule = $etudiant->getId($i);
            $codefil = $filiere2->getcodeFil();
            $etudiant->setnom($faker->firstName())
                ->setprenom($faker->lastName())
                ->setLieux($faker->city())
                ->setTelephone($faker->phoneNumber())
                ->setMatricule($codefil . '0' . $i)
                ->setDateNais($faker->dateTime())
                ->setFilieres($filiere2);
            $manager->persist($etudiant);
        }
        for ($i = 1; $i < 6; $i++) {
            $etudiant = new Etudiant();
            $matricule = $etudiant->getId($i);
            $codefil = $filiere->getcodeFil('GC');
            $etudiant->setnom($faker->firstName())
                ->setprenom($faker->lastName())
                ->setLieux($faker->city())
                ->setTelephone($faker->phoneNumber())
                ->setMatricule($codefil . '0' . $i)
                ->setDateNais($faker->dateTime())
                ->setFilieres($filiere);
            $manager->persist($etudiant);
        }
        for ($i = 1; $i < 6; $i++) {
            $etudiant = new Etudiant();
            $matricule = $etudiant->getId($i);
            $codefil = $filiere3->getcodeFil('GC');
            $etudiant->setnom($faker->firstName())
                ->setprenom($faker->lastName())
                ->setLieux($faker->city())
                ->setTelephone($faker->phoneNumber())
                ->setMatricule($codefil . '0' . $i)
                ->setDateNais($faker->dateTime())
                ->setFilieres($filiere3);
            $manager->persist($etudiant);
        }
        // $ue =  new Ue;
        // $ue->setCode('Ue01')
        //     ->setCredit(4)
        //     ->setLibelleUe('Francais');
        // $ue1 =  new Ue;
        // $ue1->setCode('Ue02')
        //     ->setCredit(5)
        //     ->setLibelleUe('Anglais');
        // $ue2 =  new Ue;
        // $ue2->setCode('Ue03')
        //     ->setCredit(6)
        //     ->setLibelleUe('Math');
        // $manager->persist($ue);
        // $manager->persist($ue1);
        // $manager->persist($ue2);

        // $evaluation = new Evaluation;
        // $evaluation->setLibelleEval('Devoir math')
        //     ->setDate($faker->dateTime());
        // $evaluation1 = new Evaluation;
        // $evaluation1->setLibelleEval('Partiel anglais')
        //     ->setDate($faker->dateTime());
        // $evaluation2 = new Evaluation;
        // $evaluation2->setLibelleEval('Devoir')
        //     ->setDate($faker->dateTime());
        // $manager->persist($evaluation);
        // $manager->persist($evaluation1);
        // $manager->persist($evaluation2);
        $manager->flush();
    }
}
