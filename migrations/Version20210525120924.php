<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210525120924 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE etudiant (id INT AUTO_INCREMENT NOT NULL, filieres_id INT NOT NULL, nom VARCHAR(25) NOT NULL, prenom VARCHAR(25) NOT NULL, lieux VARCHAR(25) NOT NULL, telephone VARCHAR(25) NOT NULL, matricule VARCHAR(50) NOT NULL, date_nais DATE NOT NULL, INDEX IDX_717E22E3A5DB2FE8 (filieres_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation (id INT AUTO_INCREMENT NOT NULL, libelle_eval VARCHAR(25) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filiere (id INT AUTO_INCREMENT NOT NULL, libelle_fil VARCHAR(25) NOT NULL, code_fil VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE note (id INT AUTO_INCREMENT NOT NULL, evaluations_id INT NOT NULL, uefilieres_id INT NOT NULL, etudiants_id INT NOT NULL, mention VARCHAR(25) NOT NULL, note20 DOUBLE PRECISION NOT NULL, INDEX IDX_CFBDFA14788B35D6 (evaluations_id), INDEX IDX_CFBDFA14D6020C09 (uefilieres_id), INDEX IDX_CFBDFA14A873A5C6 (etudiants_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE semestre (id INT AUTO_INCREMENT NOT NULL, libelle_sem VARCHAR(25) NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ue (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(25) NOT NULL, libelle_ue VARCHAR(25) NOT NULL, credit INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ue_filiere (id INT AUTO_INCREMENT NOT NULL, semestres_id INT NOT NULL, ues_id INT NOT NULL, INDEX IDX_DAA481CBF625C1D6 (semestres_id), INDEX IDX_DAA481CB5B75D6BC (ues_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE etudiant ADD CONSTRAINT FK_717E22E3A5DB2FE8 FOREIGN KEY (filieres_id) REFERENCES filiere (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14788B35D6 FOREIGN KEY (evaluations_id) REFERENCES evaluation (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14D6020C09 FOREIGN KEY (uefilieres_id) REFERENCES ue_filiere (id)');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14A873A5C6 FOREIGN KEY (etudiants_id) REFERENCES etudiant (id)');
        $this->addSql('ALTER TABLE ue_filiere ADD CONSTRAINT FK_DAA481CBF625C1D6 FOREIGN KEY (semestres_id) REFERENCES semestre (id)');
        $this->addSql('ALTER TABLE ue_filiere ADD CONSTRAINT FK_DAA481CB5B75D6BC FOREIGN KEY (ues_id) REFERENCES ue (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14A873A5C6');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14788B35D6');
        $this->addSql('ALTER TABLE etudiant DROP FOREIGN KEY FK_717E22E3A5DB2FE8');
        $this->addSql('ALTER TABLE ue_filiere DROP FOREIGN KEY FK_DAA481CBF625C1D6');
        $this->addSql('ALTER TABLE ue_filiere DROP FOREIGN KEY FK_DAA481CB5B75D6BC');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14D6020C09');
        $this->addSql('DROP TABLE etudiant');
        $this->addSql('DROP TABLE evaluation');
        $this->addSql('DROP TABLE filiere');
        $this->addSql('DROP TABLE note');
        $this->addSql('DROP TABLE semestre');
        $this->addSql('DROP TABLE ue');
        $this->addSql('DROP TABLE ue_filiere');
        $this->addSql('DROP TABLE user');
    }
}
